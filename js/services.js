'use strict';

/* Services */


// Demonstrate how to register services
var innohubOrigin = angular.module('app.services', []);

innohubOrigin.factory('AV', ['$window',
    function (win) {

        AV.initialize("4kjjc24stkt8sqkct758vc7jvv16n41m95x7n3pqcb35qtey", "vxc8wtjtyfs0a9gvhwot1d9g96s9q2y5dxrebqov1n23g0bf");
        return win.AV;
    }]);