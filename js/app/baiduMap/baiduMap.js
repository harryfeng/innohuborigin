app.controller('baiduMapCtrl', ['$scope', 'AV', function ($scope, AV) {
    var longitude = 121.506191;
    var latitude = 31.245554;
    $scope.mapOptions = {
        center: {
            longitude: longitude,
            latitude: latitude
        },
        zoom: 17,
        city: 'ShangHai',
        markers: [{
            longitude: longitude,
            latitude: latitude,
            icon: 'img/a0.jpg',
            width: 49,
            height: 60,
            title: 'Where',
            content: 'Put description here'
        }]
    };
}]);