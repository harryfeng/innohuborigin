app.controller('addRelationCtrl', ['$scope', 'AV','$location', function ($scope, AV,$location) {

    try {
        //在这里运行代码


        $location.host();

        var relationArticle = AV.Object.extend("relationArticle");
        var query = new AV.Query(relationArticle);
        query.find({
            success: function (_relationArticle) {
                $scope.articles = _relationArticle;

                $scope.$apply();
            },
            error: function (object, error) {
                debugger
                // The object was not retrieved successfully.
                // error is a AV.Error with an error code and description.
            }
        });

        var relationReader = AV.Object.extend("relationReader");
        var query = new AV.Query(relationReader);
        query.find({
            success: function (_relationReader) {
                $scope.user = _relationReader[0];
            },
            error: function (object, error) {

            }
        });

        $scope.addRelation = function (article) {
            var relation = $scope.user.relation("relationArticle");
            relation.add(article);
            $scope.user.save();
        }
    }
    catch (err) {
        console.log(err);
        //在这里处理错误
    }
    //

}]);