app.controller('userSigninCtrl', ['$scope', 'AV','$state', function ($scope, AV,$state) {

    var currentUser = AV.User.current();
    $scope.signin = function(){
        var user = AV.User.logIn($scope.username, $scope.password, {
            success: function(user) {
                $state.go('app.uploadFiles');
            },error:function(result,error){
                console.log(error);
            }
        });
    }

    $scope.signout=function(){
        AV.User.logOut();
    }
}]);