app.controller('pagnationCtrl', ['$scope', 'AV', function ($scope, AV) {

    $scope.currentPage = 1;
    $scope.pageSize = 10;
    $scope.meals = [];
    $scope.total = 0;

    $scope.count = function(){
        var HaolikeStPrinter = AV.Object.extend("keshihua");
        var haolikeStPrinter = new AV.Query(HaolikeStPrinter);
        haolikeStPrinter.count({
            success: function(count) {
                // The count request succeeded. Show the count
                $scope.total = count;
                $scope.$apply();
            },
            error: function(error) {
                // The request failed
            }
        });
    }

    $scope.pageChangeHandler = function(num) {
        var HaolikeStPrinter = AV.Object.extend("keshihua");
        var haolikeStPrinter = new AV.Query(HaolikeStPrinter);
        haolikeStPrinter.descending("createdAt");
        haolikeStPrinter.limit(10);
        haolikeStPrinter.skip(10*(num-1));
        haolikeStPrinter.find({
            success : function(haolikeStPrinter) {
                $scope.printers = haolikeStPrinter;
                $scope.$apply();
            },
            error : function(error) {
                console.log(error.message);
            }
        });
    };
    $scope.pageChangeHandler(1);
}]);