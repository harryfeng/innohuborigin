app.controller('keshihuobianyiCtrl', ['$scope', 'AV','$location', function ($scope, AV,$location) {
   // $scope.htmlString ='I am an <code>HTML</code>string with ' +'<a href="#">links!</a> and other <em>stuff</em>';
    var options = {
        uploadJson: '/uploadImg'
    };
    KindEditor.ready(function(K) {
        window.editor = K.create('#editor_id', options);
    });

    $scope.getHTML = function(){
        html = editor.html();
        var GameScore = AV.Object.extend("keshihua");
        var gameScore = new GameScore();
        gameScore.set("html", html);
        gameScore.save(null, {
            success: function(gameScore) {
                // Execute any logic that should take place after the object is saved.
                console.log(gameScore.attributes.html);
                $scope.htmlString =gameScore.attributes.html;

                $scope.$apply();
            },
            error: function(gameScore, error) {
                // Execute any logic that should take place if the save fails.
                // error is a AV.Error with an error code and description.
                alert('Failed to create new object, with error code: ' + error.message);
            }
        });
    }

}]);