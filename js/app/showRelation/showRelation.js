app.controller('showRelationCtrl', ['$scope', 'AV', function ($scope, AV) {
    var relationReader = AV.Object.extend("relationReader");
    var query = new AV.Query(relationReader);
    query.find({
        success: function(_relationReader) {
            var relation = _relationReader[0].relation("relationArticle");
            relation.query().find({
                success: function(list) {
                    $scope.articles = list;
                    $scope.$apply();
                },error:function(result,err){
                    debugger
                }
            });


            // The object was retrieved successfully.
        },
        error: function(object, error) {
            debugger
            // The object was not retrieved successfully.
            // error is a AV.Error with an error code and description.
        }
    });
    //

}]);