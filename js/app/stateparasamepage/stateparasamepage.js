app.controller('stateparasamepageCtrl', ['$scope', 'AV', function ($scope, AV) {
    var relationArticleType = AV.Object.extend("relationArticleType");
    var query = new AV.Query(relationArticleType);
    query.find({
        success: function(_relationArticleType) {
            $scope.articleTypes = _relationArticleType;
            $scope.$apply();

        },
        error: function(object, error) {
            debugger
            // The object was not retrieved successfully.
            // error is a AV.Error with an error code and description.
        }
    });

    $scope.clickType = function(type){
        var relation = type.relation("articles");
        relation.query().find({
            success: function(list) {
                $scope.articles = list;
                $scope.$apply();
            },error:function(result,err){
                debugger
            }
        });
    };

    $scope.clickArticle = function(article){
        $scope.content = article.attributes.articleContent;
    }

}]);