app.controller('repeatNgClassCtrl', ['$scope', 'AV', function ($scope, AV) {
    var ngClass = AV.Object.extend("ngClass");
    var query = new AV.Query(ngClass);
    query.descending('createdAt');
    query.find({
        success: function(_ngClass) {
            $scope.classes = _ngClass;
            $scope.$apply();
            // The object was retrieved successfully.
        },
        error: function(object, error) {
            debugger
            // The object was not retrieved successfully.
            // error is a AV.Error with an error code and description.
        }
    });

}]);