app.controller('MapCtrl', ['$scope', 'AV', function ($scope, AV) {

    $scope.mapOptions = {
        mapType: BMap.MapType.BMAP_PERSPECTIVE_MAP,
        // ui map config
        ngCenter: {
            lat: 22.505854,
            lng: 113.382254
        },
        ngZoom: 25
    };
}]);