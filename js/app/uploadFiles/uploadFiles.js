app.controller('uploadFilesCtrl', ['$scope', 'AV','$state', function ($scope, AV,$state) {

    if(AV.User.current()==null){

        $state.go('app.userSignin');
    }

    var relationReader = AV.Object.extend("relationReader");
    var query = new AV.Query(relationReader);
    query.find({
        success: function (_relationReader) {
            $scope.user = _relationReader[0];
        },
        error: function (object, error) {
            debugger
            // The object was not retrieved successfully.
            // error is a AV.Error with an error code and description.
        }
    });


    $scope.saveFile = function () {
        var fileUploadControl = $("#profilePhotoFileUpload")[0];
        if (fileUploadControl.files.length > 0) {
            var file = fileUploadControl.files[0];
            var name = "photo.jpg";

            var avFile = new AV.File(name, file);
            avFile.save().then(function (_file) {
                $scope.user.set('userProfile', avFile);
                $scope.user.save(null, {
                    success: function (user) {
                        debugger
                    }, error: function (result, err) {
                        debugger
                    }
                });
            }, function (error) {
                // The file either could not be read, or could not be saved to AV.
            });
            ;
        }
    }
}]);